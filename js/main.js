$(document).ready(function(){
	var url = window.location.pathname;
	var filename = url.substring(url.lastIndexOf('/')+1);
	if (filename != "index.html") {
		$(".navbar").addClass("active");
	}
	else {
		$(".navbar").addClass("transparent");
	}
	$(window).scroll(function(){
		var scroll = $(window).scrollTop();
		if (scroll > 70) {
			$(".navbar").removeClass("transparent");
			$(".navbar").addClass("active");
		}
		else if (filename == "index.html"){
			$(".navbar").removeClass("active");
			$(".navbar").addClass("transparent");
		}
	})
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	})

	// index.html widget passengers func

	$(document).ready(function () {
		// $(".inputPassengersMaxLength").attr('maxlength', '1');
		$(".inputPassengersMaxLength").val(this.value.match(/[0-9]*/));  
	});
});

